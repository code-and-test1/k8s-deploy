#!/bin/sh

# DEFINED
IMAGE_NAME=$1
IMAGE_VERSION=$2

echo "[BUILD_IMAGE_NAME]: $IMAGE_NAME"
echo "[VERSION]: $IMAGE_VERSION"

if [ -z $IMAGE_NAME ] || [ -z $IMAGE_VERSION ]; then
    echo "[ERROR] missing args"
    exit 1
fi

alias d="docker"

# DOCKER BUILD PUSH

##BUILD
echo "STARTING BUILD $IMAGE_NAME:$IMAGE_VERSION $IMAGE_NAME:latest"
d build -t uetdockerhub.vn:5000/$IMAGE_NAME:$IMAGE_VERSION -t uetdockerhub.vn:5000/$IMAGE_NAME:latest .

##PUSH
echo "STARTING PUSH uetdockerhub.vn:5000/$IMAGE_NAME:$IMAGE_VERSION uetdockerhub.vn:5000/$IMAGE_NAME:latest"

d push uetdockerhub.vn:5000/$IMAGE_NAME:$IMAGE_VERSION
d push uetdockerhub.vn:5000/$IMAGE_NAME:$IMAGE_VERSION

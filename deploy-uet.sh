#!/bin/sh

# DEFINED
IMAGE_NAME=$1
IMAGE_VERSION=$2

echo "[DEPLOY_IMAGE_NAME]: $IMAGE_NAME"
echo "[VERSION]: $IMAGE_VERSION"

if [ -z $IMAGE_NAME ] || [ -z $IMAGE_VERSION ]; then
    echo "[ERROR] missing args"
    exit 1
fi

# DEPLOY K8s
echo "STARTING DEPLOY $IMAGE_NAME:$IMAGE_VERSION"

export IMAGE_NAME
export IMAGE_VERSION
envsubst < deployment-uet.yml | kubectl apply -f -

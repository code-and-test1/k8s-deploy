#!/bin/sh

# Clear
echo "[CLEAN_OLD_SERVER]"
helm uninstall nginx-ingress --namespace=nginx-ingress
kubectl delete namespace codeatest

# Create namespace, service account, secret, config,...
echo "[CREATE_NS_SA_SECRET_CONFIG]"
kubectl apply -f ns-sa.yml

# Install nginx-ingress controller
echo "[INSTALL NGINX-INGRESS CONTROLLER]"
helm install nginx-ingress --namespace=nginx-ingress nginx-stable/nginx-ingress --set rbac.create=true --set controller.publishService.enabled=true

# Create Ingress Master
echo "[CREATE_INGRESS_MASTER]"
kubectl apply -f ingress.yml

echo "[BOOTSTRAP_DONE]"

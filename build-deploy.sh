#!/bin/sh

# DEFINED
IMAGE_NAME=$1
IMAGE_VERSION=$2

alias deploy="~/k8s-deploy/deploy.sh"
alias build="~/k8s-deploy/build.sh"

build $IMAGE_NAME $IMAGE_VERSION
deploy $IMAGE_NAME $IMAGE_VERSION

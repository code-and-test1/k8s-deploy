#!/bin/sh

# DEFINED
IMAGE_NAME=$1
IMAGE_VERSION=$2

echo "[BUILD_IMAGE_NAME]: $IMAGE_NAME"
echo "[VERSION]: $IMAGE_VERSION"

if [ -z $IMAGE_NAME ] || [ -z $IMAGE_VERSION ]; then
    echo "[ERROR] missing args"
    exit 1
fi

alias d="sudo docker"

# DOCKER BUILD PUSH

##BUILD
echo "STARTING BUILD $IMAGE_NAME:$IMAGE_VERSION $IMAGE_NAME:latest"
d build -t fieubat2x/$IMAGE_NAME:$IMAGE_VERSION -t fieubat2x/$IMAGE_NAME:latest .

##PUSH
echo "STARTING PUSH fieubat2x/$IMAGE_NAME:$IMAGE_VERSION fieubat2x/$IMAGE_NAME:latest"

d push fieubat2x/$IMAGE_NAME:$IMAGE_VERSION
d push fieubat2x/$IMAGE_NAME:$IMAGE_VERSION
